package com.saurabh.article.app.models.viewmodels;

import com.saurabh.article.app.R;
import com.saurabh.article.app.controllers.constants.Constants;
import com.saurabh.article.app.controllers.contracts.ResourceMapper;
import com.saurabh.article.app.controllers.logger.AppLogger;
import com.saurabh.article.app.controllers.network.RemoteDataSource;
import com.saurabh.article.app.models.responses.PopularResponse;

import javax.inject.Inject;

import androidx.databinding.ObservableArrayList;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import androidx.databinding.ObservableList;
import androidx.lifecycle.ViewModel;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import static com.saurabh.article.app.controllers.constants.Constants.Most_Emailed;
import static com.saurabh.article.app.controllers.constants.Constants.Most_Shared;
import static com.saurabh.article.app.controllers.constants.Constants.Most_Viewed;

public class PopularViewModel extends ViewModel {

    private RemoteDataSource remoteDataSource;
    private ResourceMapper resourceMapper;
    private CompositeDisposable disposable;

    /**
     * @Observables these are the fields that our views are consuming and observing for any change.
     *              It directly updates them into the XML.
     * */
    public final ObservableBoolean popularLoaderVisibility = new ObservableBoolean(false);
    public final ObservableBoolean popularListVisibility = new ObservableBoolean(false);
    public final ObservableBoolean noPopularTextVisibility = new ObservableBoolean(false);
    public final ObservableField<Integer> emailTabColor = new ObservableField<>();
    public final ObservableField<Integer> sharedTabColor = new ObservableField<>();
    public final ObservableField<Integer> viewedTabColor = new ObservableField<>();
    /**
     * List that we directly sends data to adapter to update
     * */
    public final ObservableList<PopularResponse.PopularResults> popularArticles = new ObservableArrayList<>();

    @Inject
    public PopularViewModel(RemoteDataSource remoteDataSource, ResourceMapper resourceMapper) {
        this.remoteDataSource = remoteDataSource;
        this.resourceMapper = resourceMapper;
        disposable = new CompositeDisposable();
        emailTabColor.set(resourceMapper.getColor(R.color.blue));
        sharedTabColor.set(resourceMapper.getColor(R.color.black));
        viewedTabColor.set(resourceMapper.getColor(R.color.black));
    }

    public void loadArticles(@Constants.ArticleType int type) {
        disposable.clear();
        switch (type) {
            case Most_Emailed:
                remoteDataSource.getMostEmailed()
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .doOnSubscribe(dispose -> disposable.add(dispose))
                        .subscribe(articleObserver);
                break;
            case Most_Shared:
                remoteDataSource.getMostShared()
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .doOnSubscribe(dispose -> disposable.add(dispose))
                        .subscribe(articleObserver);
                break;
            case Most_Viewed:
                remoteDataSource.getMostViewed()
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .doOnSubscribe(dispose -> disposable.add(dispose))
                        .subscribe(articleObserver);
                break;
        }
    }

    private Observer<PopularResponse> articleObserver = new Observer<PopularResponse>() {
        @Override
        public void onSubscribe(Disposable d) {
            AppLogger.log("onSubscribe");
            popularListVisibility.set(false);
            popularLoaderVisibility.set(true);
            noPopularTextVisibility.set(false);
        }

        @Override
        public void onNext(PopularResponse popularResponse) {
            AppLogger.log("onNext: articles = " + popularResponse.getNum_results());
            popularListVisibility.set(popularResponse.getNum_results() > 0);
            popularLoaderVisibility.set(!(popularResponse.getNum_results() > 0));
            noPopularTextVisibility.set(!(popularResponse.getNum_results() > 0));
            if (popularResponse.getNum_results() > 0) {
                popularArticles.clear();
                popularArticles.addAll(popularResponse.getResults());
            }
        }

        @Override
        public void onError(Throwable e) {
            AppLogger.log("error: " + e.getMessage());
            popularListVisibility.set(false);
            popularLoaderVisibility.set(false);
            noPopularTextVisibility.set(true);
        }

        @Override
        public void onComplete() {
            AppLogger.log("onComplete");
        }
    };

    public void onMostEmailedTabClick() {
        emailTabColor.set(resourceMapper.getColor(R.color.blue));
        sharedTabColor.set(resourceMapper.getColor(R.color.black));
        viewedTabColor.set(resourceMapper.getColor(R.color.black));
        loadArticles(Most_Emailed);
    }

    public void onMostSharedTabClick() {
        emailTabColor.set(resourceMapper.getColor(R.color.black));
        sharedTabColor.set(resourceMapper.getColor(R.color.blue));
        viewedTabColor.set(resourceMapper.getColor(R.color.black));
        loadArticles(Most_Shared);
    }

    public void onMostViewedTabClick() {
        emailTabColor.set(resourceMapper.getColor(R.color.black));
        sharedTabColor.set(resourceMapper.getColor(R.color.black));
        viewedTabColor.set(resourceMapper.getColor(R.color.blue));
        loadArticles(Most_Viewed);
    }

    @Override
    protected void onCleared() {
        disposable.dispose();
        super.onCleared();
    }
}

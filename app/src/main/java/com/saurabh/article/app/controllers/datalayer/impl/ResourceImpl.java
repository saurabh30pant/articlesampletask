package com.saurabh.article.app.controllers.datalayer.impl;

import android.content.Context;
import android.graphics.drawable.Drawable;

import com.saurabh.article.app.controllers.contracts.ResourceMapper;

import javax.inject.Inject;

public class ResourceImpl implements ResourceMapper {

    private Context context;

    @Inject
    public ResourceImpl(Context context) {
        this.context = context;
    }

    @Override
    public String getString(int resId) {
        return context.getResources().getString(resId);
    }

    @Override
    public int getColor(int resId) {
        return context.getResources().getColor(resId);
    }

    @Override
    public Drawable getDrawable(int resId) {
        return context.getResources().getDrawable(resId);
    }
}

package com.saurabh.article.app.controllers.network;

import com.google.gson.JsonObject;
import com.saurabh.article.app.controllers.constants.Constants;
import com.saurabh.article.app.models.responses.PopularResponse;
import com.saurabh.article.app.models.responses.SearchResponse;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface API {

    @GET("search/v2/articlesearch.json?api-key=" + Constants.ApiKey)
    Observable<SearchResponse> search(@Query("q") String keyword);

    @GET("mostpopular/v2/emailed/1.json?api-key=" + Constants.ApiKey)
    Observable<PopularResponse> mostEmailed();

    @GET("mostpopular/v2/shared/1/facebook.json?api-key=" + Constants.ApiKey)
    Observable<PopularResponse> mostShared();

    @GET("mostpopular/v2/viewed/1.json?api-key=" + Constants.ApiKey)
    Observable<PopularResponse> mostViewed();
}
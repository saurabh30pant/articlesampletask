package com.saurabh.article.app.controllers.constants;

public enum ProgressState {

    Loading(101), Success(102), Failure(103), Error(104), Complete(105);

    private int stateId;

    ProgressState(int id) {
        stateId = id;
    }

    public int getStateId() {
        return stateId;
    }
}

package com.saurabh.article.app.ui.activities;

import android.os.Bundle;
import android.view.View;

import com.saurabh.article.app.R;
import com.saurabh.article.app.ui.app_views.AppIconView;
import com.saurabh.article.app.ui.fragments.PopularFragment;
import com.saurabh.article.app.ui.fragments.SearchFragment;

import androidx.annotation.Nullable;

public class ArticleListActivity extends BaseActivity {

    private AppIconView searchIcon;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        searchIcon = findViewById(R.id.view_search_icon);
        searchIcon.setOnClickListener(view -> {
            searchIcon.setVisibility(View.GONE);
            onSearchClick();
        });

        PopularFragment popularFragment =
                (PopularFragment) getSupportFragmentManager().findFragmentByTag(PopularFragment.class.getName());
        if (popularFragment == null)
            popularFragment = PopularFragment.instance();
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.frame_content, popularFragment, PopularFragment.class.getName())
                .commitAllowingStateLoss();
    }

    public void onSearchClick() {
        SearchFragment searchFragment =
                (SearchFragment) getSupportFragmentManager().findFragmentByTag(SearchFragment.class.getName());
        if (searchFragment == null)
            searchFragment = SearchFragment.getInstance();
        getSupportFragmentManager().beginTransaction()
                .add(R.id.frame_content, searchFragment, SearchFragment.class.getName())
                .addToBackStack(SearchFragment.class.getName())
                .commitAllowingStateLoss();
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() == 1) {
            getSupportFragmentManager().popBackStackImmediate();
            searchIcon.setVisibility(View.VISIBLE);
        } else {
            super.onBackPressed();
            finish();
        }
    }

    @Override
    protected int getActivityLayout() {
        return R.layout.activity_list;
    }
}

package com.saurabh.article.app.ui.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.saurabh.article.app.controllers.constants.Constants;
import com.saurabh.article.app.databinding.FragmentPopularBinding;
import com.saurabh.article.app.dependency.ApplicationClass;
import com.saurabh.article.app.models.viewmodels.PopularViewModel;
import com.saurabh.article.app.ui.adapters.PopularArticleListAdapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class PopularFragment extends BaseFragment {

    private PopularViewModel viewModel;
    private FragmentPopularBinding viewBinding;
    private RecyclerView popularListView;

    public static PopularFragment instance() {
        PopularFragment fragment = new PopularFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        /**
         *  Informing dagger that we are inject dependencies in this class.
         *  */
        ((ApplicationClass) getActivity().getApplicationContext()).getAppcomponent().inject(this);
        /**
         * @networkSelectionViewModel getting it from the ViewModelFactory object and directly injecting the required dependencies
         *                        in it via Dagger.
         * */
        viewModel = ViewModelProviders.of(getActivity(), viewModelFactory).get(PopularViewModel.class);
        /**
         * @viewBinding binding the fragment with view via data binding.
         * */
        viewBinding = FragmentPopularBinding.inflate(inflater, container, false);
        viewBinding.setViewModel(viewModel);

        popularListView = viewBinding.listPopularArticles;
        popularListView.setLayoutManager(new LinearLayoutManager(getContext()));
        popularListView.setAdapter(new PopularArticleListAdapter());

        return viewBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewModel.loadArticles(Constants.Most_Emailed);
    }
}

package com.saurabh.article.app.ui.app_views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.Gravity;

import androidx.appcompat.widget.AppCompatTextView;

import static com.saurabh.article.app.ui.app_views.AppFont.Font_App;

public class AppIconView extends AppCompatTextView {

	private Context c;

	public AppIconView(Context context) {
		super(context);
		c = context;
		init();
	}

	public AppIconView(Context context, AttributeSet attrs) {
		super(context, attrs);
		c = context;
		init();
	}

	private void init() {
		setGravity(Gravity.CENTER);
		setTypeface(FontTypeface.get(Font_App, c));
	}

}

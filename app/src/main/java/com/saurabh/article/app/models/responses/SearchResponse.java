package com.saurabh.article.app.models.responses;

import java.util.List;

public class SearchResponse extends BaseResponse{

    private Response response;

    public Response getResponse() {
        return response;
    }

    public void setResponse(Response response) {
        this.response = response;
    }

    public class Response {
        private List<SearchQuery> docs;

        public List<SearchQuery> getDocs() {
            return docs;
        }

        public void setDocs(List<SearchQuery> docs) {
            this.docs = docs;
        }
    }

    public class SearchQuery {

        private String web_url, snippet, source, pub_date;
        private Headline headline;

        public String getWeb_url() {
            return web_url;
        }

        public void setWeb_url(String web_url) {
            this.web_url = web_url;
        }

        public String getSnippet() {
            return snippet;
        }

        public void setSnippet(String snippet) {
            this.snippet = snippet;
        }

        public String getSource() {
            return source;
        }

        public void setSource(String source) {
            this.source = source;
        }

        public String getPub_date() {
            return pub_date;
        }

        public void setPub_date(String pub_date) {
            this.pub_date = pub_date;
        }

        public Headline getHeadline() {
            return headline;
        }

        public void setHeadline(Headline headline) {
            this.headline = headline;
        }
    }

    public class Headline {
        private String  main;

        public String getMain() {
            return main;
        }

        public void setMain(String main) {
            this.main = main;
        }
    }
}

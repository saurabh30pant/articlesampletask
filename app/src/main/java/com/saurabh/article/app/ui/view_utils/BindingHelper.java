package com.saurabh.article.app.ui.view_utils;

import android.content.res.Resources;
import android.graphics.drawable.GradientDrawable;
import android.view.View;
import android.widget.ProgressBar;

import com.saurabh.article.app.R;
import com.saurabh.article.app.models.responses.PopularResponse;
import com.saurabh.article.app.models.responses.SearchResponse;
import com.saurabh.article.app.ui.adapters.PopularArticleListAdapter;
import com.saurabh.article.app.ui.adapters.SearchArticleListAdapter;
import com.saurabh.article.app.ui.app_views.AppEditView;
import com.saurabh.article.app.ui.app_views.AppTextView;

import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.util.List;
import java.util.UUID;

import androidx.databinding.BindingAdapter;
import androidx.recyclerview.widget.RecyclerView;

import static com.saurabh.article.app.ui.view_utils.TimeManager.DateTimeFormat;

public class BindingHelper {

    @BindingAdapter("search_list_items")
    public static void setSearchItems(RecyclerView recyclerView, List<SearchResponse.SearchQuery> devices) {
        SearchArticleListAdapter searchArticleListAdapter = (SearchArticleListAdapter) recyclerView.getAdapter();
        if (searchArticleListAdapter != null) {
            searchArticleListAdapter.updateDevices(devices);
        }
    }

    @BindingAdapter("popular_list_items")
    public static void setPopularItems(RecyclerView recyclerView, List<PopularResponse.PopularResults> devices) {
        PopularArticleListAdapter popularArticleListAdapter = (PopularArticleListAdapter) recyclerView.getAdapter();
        if (popularArticleListAdapter != null) {
            popularArticleListAdapter.updateDevices(devices);
        }
    }

    @BindingAdapter("tab_text_color")
    public static void setBackgroundDrawable(AppTextView view, int color) {
        view.setTextColor(color);
    }

    @BindingAdapter("view_visibility")
    public static void setViewVisibility(ProgressBar view, boolean isVisible) {
        view.setVisibility(isVisible ? View.VISIBLE : View.GONE);
    }

    @BindingAdapter("input_type")
    public static void setInputType(AppEditView editView, boolean passwordType) {
        editView.setInputAsPassword(passwordType);
    }

    @BindingAdapter("set_selection")
    public static void setSelectionOnEdit(AppEditView editView, int pwdLength) {
        editView.setSelection(editView.getText().toString().length());
    }

    @BindingAdapter("device_item_background")
    public static void setDeviceItemBackground(View view, int color) {
        GradientDrawable itemDrawable = (GradientDrawable) view.getBackground();
        itemDrawable.setColor(color);
    }

    @BindingAdapter("device_next_step_background")
    public static void setDeviceNextButtonBackground(View view, int color) {
        GradientDrawable nextButtonDrawable = (GradientDrawable) view.getBackground();
        nextButtonDrawable.setColor(color);
    }

    @BindingAdapter("hidden_network_connect_background")
    public static void setHiddenConnectButtonBackground(View view, int color) {
        GradientDrawable nextButtonDrawable = (GradientDrawable) view.getBackground();
        nextButtonDrawable.setColor(color);
    }

    public static Long generateUniqueId()
    {
        long val = -1;
        do
        {
            final UUID uid = UUID.randomUUID();
            final ByteBuffer buffer = ByteBuffer.wrap(new byte[16]);
            buffer.putLong(uid.getLeastSignificantBits());
            buffer.putLong(uid.getMostSignificantBits());
            final BigInteger bi = new BigInteger(buffer.array());
            val = bi.longValue();
        } while (val < 0);
        return val;
    }
}

package com.saurabh.article.app.ui.adapters;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.saurabh.article.app.databinding.LayoutItemSearchBinding;
import com.saurabh.article.app.models.responses.SearchResponse;
import com.saurabh.article.app.models.viewmodels.SearchViewModel;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class SearchArticleListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<SearchResponse.SearchQuery> articles = new ArrayList<>();

    public SearchArticleListAdapter() {}

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutItemSearchBinding itemBinding = LayoutItemSearchBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new ViewHolder(itemBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        ViewHolder holder = (ViewHolder) viewHolder;
        holder.itemBinding.setArticle(articles.get(position));
        holder.itemBinding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return articles.size();
    }

    public void updateDevices(List<SearchResponse.SearchQuery> articles) {
        this.articles = articles;
        notifyDataSetChanged();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        final LayoutItemSearchBinding itemBinding;

        public ViewHolder(LayoutItemSearchBinding itemBinding) {
            super(itemBinding.getRoot());
            this.itemBinding = itemBinding;
        }
    }
}
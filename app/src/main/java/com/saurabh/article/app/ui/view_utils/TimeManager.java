package com.saurabh.article.app.ui.view_utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class TimeManager {

    public static final String HourMinFormat = "hh:mm aa";
    public static final String DateTimeFormat = "dd MMM hh:mm aa";

    public static String getTimeFromMillis(long timeMillis, String expectedDateFormat) {
        DateFormat dateFormat = new SimpleDateFormat(expectedDateFormat);
        return dateFormat.format(timeMillis);
    }
}

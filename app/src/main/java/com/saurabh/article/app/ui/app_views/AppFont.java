package com.saurabh.article.app.ui.app_views;


/**
 * Created by Saurabh(aqua) on 02-11-2016.
 */

public class AppFont {

    /*
     * Custom fonts typeface
     * */
    public static final String Font_App = "icon_font.otf";
    public static final String Font_Regular = "font_regular.ttf";
    public static final String Font_Light = "font_light.ttf";
    public static final String Font_Bold = "font_bold.ttf";

}

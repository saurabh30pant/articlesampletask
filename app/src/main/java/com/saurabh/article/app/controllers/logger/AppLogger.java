package com.saurabh.article.app.controllers.logger;

import android.util.Log;


public class AppLogger {

    public static void log(String message) {
            Log.e(AppLogger.class.getName(), message);
    }

}

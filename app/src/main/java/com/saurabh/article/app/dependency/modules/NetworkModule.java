package com.saurabh.article.app.dependency.modules;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.saurabh.article.app.controllers.network.API;
import com.saurabh.article.app.controllers.network.RemoteDataSource;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.saurabh.article.app.controllers.constants.Constants.BaseUrl;

@Module
public class NetworkModule {

    private String base_url;

    public NetworkModule() {
        this.base_url = BaseUrl;
    }

    @Singleton
    @Provides
    public GsonConverterFactory getConverterFactory(){
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        return GsonConverterFactory.create(gson);
    }

    @Singleton
    @Provides
    public Retrofit retrofitInstance(GsonConverterFactory converter_factory) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(base_url)
                .addConverterFactory(converter_factory)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
        return retrofit;
    }

    @Singleton
    @Provides
    public API getApi(Retrofit retrofit){
        return retrofit.create(API.class);
    }

    @Singleton
    @Provides
    public RemoteDataSource getRemoteSource(API api) {
        return new RemoteDataSource(api);
    }

}

package com.saurabh.article.app.controllers.logger;

import android.content.Context;
import android.widget.Toast;

public class ToastClass {

    public static void showToast(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

}

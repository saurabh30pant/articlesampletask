package com.saurabh.article.app.dependency.modules;

import android.app.Application;
import android.content.Context;

import com.saurabh.article.app.controllers.contracts.ResourceMapper;
import com.saurabh.article.app.controllers.datalayer.impl.ResourceImpl;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {

    private Application mApplication;

    public AppModule(Application application) {
        mApplication = application;
    }

    @Provides
    @Singleton
    public Context getApplicationContext() {
        return mApplication;
    }

    @Provides
    @Singleton
    public ResourceMapper resourceMapper(Context context) {
        return new ResourceImpl(context);
    }

}
package com.saurabh.article.app.ui.adapters;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.saurabh.article.app.databinding.LayoutItemPopularBinding;
import com.saurabh.article.app.databinding.LayoutItemSearchBinding;
import com.saurabh.article.app.models.responses.PopularResponse;
import com.saurabh.article.app.models.responses.SearchResponse;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class PopularArticleListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<PopularResponse.PopularResults> articles = new ArrayList<>();

    public PopularArticleListAdapter() {}

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutItemPopularBinding itemBinding = LayoutItemPopularBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new ViewHolder(itemBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        ViewHolder holder = (ViewHolder) viewHolder;
        holder.itemBinding.setArticle(articles.get(position));
        holder.itemBinding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return articles.size();
    }

    public void updateDevices(List<PopularResponse.PopularResults> articles) {
        this.articles = articles;
        notifyDataSetChanged();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        final LayoutItemPopularBinding itemBinding;

        public ViewHolder(LayoutItemPopularBinding itemBinding) {
            super(itemBinding.getRoot());
            this.itemBinding = itemBinding;
        }
    }
}
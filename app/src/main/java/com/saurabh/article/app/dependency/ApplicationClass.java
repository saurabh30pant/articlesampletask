package com.saurabh.article.app.dependency;

import android.app.Application;

import com.saurabh.article.app.dependency.modules.AppModule;

public class ApplicationClass extends Application {

    private static AppComponent app_component;

    @Override
    public void onCreate() {
        super.onCreate();
        app_component = DaggerAppComponent.builder().appModule(new AppModule(this)).build();
    }

    public static AppComponent getAppcomponent() {
        return app_component;
    }

}

package com.saurabh.article.app.ui.app_views;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;


import com.saurabh.article.app.R;

import androidx.appcompat.widget.AppCompatTextView;

import static com.saurabh.article.app.ui.app_views.AppFont.Font_Bold;
import static com.saurabh.article.app.ui.app_views.AppFont.Font_Light;
import static com.saurabh.article.app.ui.app_views.AppFont.Font_Regular;

/**
 * Created by Saurabh(aqua) on 21-03-2017.
 */

public class AppTextView extends AppCompatTextView {

    private Context context;

    public static final int RegularFont = 1;
    public static final int BoldFont = 2;
    public static final int LightFont = 3;

    private int fontType = RegularFont;

    public AppTextView(Context context) {
        super(context);
        this.context = context;
        createView();
    }

    public AppTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        TypedArray ta = getContext().obtainStyledAttributes(attrs, R.styleable.textview_font_type);
        fontType = ta.getInt(R.styleable.textview_font_type_font_type, RegularFont);
        createView();
    }

    private void createView() {
        setTypeface(FontTypeface.get(Font_Regular, context));
        setTextStyle(fontType);
    }

    public void setTextStyle(int code){
        switch (code) {
            case RegularFont:
                setTypeface(FontTypeface.get(Font_Regular, context));
                break;
            case BoldFont:
                setTypeface(FontTypeface.get(Font_Bold, context));
                break;
            case LightFont:
                setTypeface(FontTypeface.get(Font_Light, context));
                break;
        }
    }

}

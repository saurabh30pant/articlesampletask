package com.saurabh.article.app.models.viewmodels;

import com.saurabh.article.app.controllers.logger.AppLogger;
import com.saurabh.article.app.controllers.network.RemoteDataSource;
import com.saurabh.article.app.models.responses.SearchResponse;
import com.saurabh.article.app.ui.view_utils.SingleLiveEvent;

import javax.inject.Inject;

import androidx.databinding.ObservableArrayList;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableList;
import androidx.lifecycle.ViewModel;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.Subject;

public class SearchViewModel extends ViewModel {

    private RemoteDataSource remoteDataSource;
    private String searchKeyword = "";
    private CompositeDisposable disposable;

    /**
     * @Observables these are the fields that our views are consuming and observing for any change.
     *              It directly updates them into the XML.
     * */
    public final ObservableBoolean searchLoaderVisibility = new ObservableBoolean(false);
    public final ObservableBoolean searchListVisibility = new ObservableBoolean(false);
    public final ObservableBoolean noSearchTextVisibility = new ObservableBoolean(false);
    public final ObservableBoolean searchButtonVisibility = new ObservableBoolean(false);
    /**
     * List that we directly sends data to adapter to update
     * */
    public final ObservableList<SearchResponse.SearchQuery> searchedArticles = new ObservableArrayList<>();

    @Inject
    public SearchViewModel(RemoteDataSource remoteDataSource) {
        this.remoteDataSource = remoteDataSource;
        disposable = new CompositeDisposable();
    }

    public void onArticleSearchClick() {
        remoteDataSource.search(searchKeyword)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(dispose -> disposable.add(dispose))
                .subscribe(new Observer<SearchResponse>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        AppLogger.log("onSubscribe");
                        searchListVisibility.set(false);
                        searchLoaderVisibility.set(true);
                        noSearchTextVisibility.set(false);
                        searchButtonVisibility.set(false);
                    }

                    @Override
                    public void onNext(SearchResponse searchResponse) {
                        AppLogger.log("onNext: search articles = " + searchResponse.getNum_results());
                        searchListVisibility.set(searchResponse.getResponse().getDocs().size() > 0);
                        searchLoaderVisibility.set(!(searchResponse.getResponse().getDocs().size() > 0));
                        noSearchTextVisibility.set(!(searchResponse.getResponse().getDocs().size() > 0));
                        searchButtonVisibility.set(true);
                        if (searchResponse.getResponse().getDocs().size() > 0) {
                            searchedArticles.clear();
                            searchedArticles.addAll(searchResponse.getResponse().getDocs());
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        searchListVisibility.set(false);
                        searchLoaderVisibility.set(false);
                        noSearchTextVisibility.set(true);
                        searchButtonVisibility.set(true);
                    }

                    @Override
                    public void onComplete() {
                        AppLogger.log("onComplete");
                    }
                });
    }

    /**
     * @setQRCodeObserver setting the text change listener on the edit text field. Here it's the QR code edit field.
     * */
    public void setSearchObserver(Subject<String> searchField) {
        searchField.doOnSubscribe(dispose -> disposable.add(dispose))
                .subscribe(searchObserver);
    }

    private Observer<String> searchObserver = new Observer<String>() {
        @Override
        public void onSubscribe(Disposable d) {}

        @Override
        public void onNext(String word) {
            searchKeyword = word;
            searchButtonVisibility.set(searchKeyword.length() > 0);
        }

        @Override
        public void onError(Throwable e) {
            AppLogger.log("QR code change observer error: " + e.getMessage());
        }

        @Override
        public void onComplete() {}
    };

    @Override
    protected void onCleared() {
        disposable.clear();
        super.onCleared();
    }
}

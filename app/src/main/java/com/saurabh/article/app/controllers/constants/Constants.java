package com.saurabh.article.app.controllers.constants;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import androidx.annotation.IntDef;

public class Constants {

    public static final String BaseUrl = "https://api.nytimes.com/svc/";
    public static final String ApiKey = "44e9MgsEeaafZOX3qTMX5DTRMhViomjd";

    public static final int Most_Emailed = 11;
    public static final int Most_Shared = 22;
    public static final int Most_Viewed = 33;

    @Retention(RetentionPolicy.SOURCE)
    @IntDef({Most_Emailed, Most_Shared, Most_Viewed})
    public @interface ArticleType{}
}
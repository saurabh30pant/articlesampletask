package com.saurabh.article.app.models.responses;

import java.util.List;

public class PopularResponse extends BaseResponse {

    private List<PopularResults> results;

    public List<PopularResults> getResults() {
        return results;
    }

    public void setResults(List<PopularResults> results) {
        this.results = results;
    }

    public class PopularResults {

        private String url, id, title, published_date, source, byline;

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getPublished_date() {
            return published_date;
        }

        public void setPublished_date(String published_date) {
            this.published_date = published_date;
        }

        public String getSource() {
            return source;
        }

        public void setSource(String source) {
            this.source = source;
        }

        public String getByline() {
            return byline;
        }

        public void setByline(String byline) {
            this.byline = byline;
        }
    }

}
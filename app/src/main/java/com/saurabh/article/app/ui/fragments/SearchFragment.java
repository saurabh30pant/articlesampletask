package com.saurabh.article.app.ui.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.saurabh.article.app.databinding.FragmentSearchBinding;
import com.saurabh.article.app.dependency.ApplicationClass;
import com.saurabh.article.app.models.viewmodels.SearchViewModel;
import com.saurabh.article.app.ui.adapters.SearchArticleListAdapter;
import com.saurabh.article.app.ui.app_views.AppEditView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class SearchFragment extends BaseFragment {

    private AppEditView searchEditView;
    private RecyclerView searchListView;
    private FragmentSearchBinding viewBinding;
    private SearchViewModel viewModel;

    public static SearchFragment getInstance() {
        SearchFragment fragment = new SearchFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        /**
         *  Informing dagger that we are inject dependencies in this class.
         *  */
        ((ApplicationClass) getActivity().getApplicationContext()).getAppcomponent().inject(this);
        /**
         * @networkSelectionViewModel getting it from the ViewModelFactory object and directly injecting the required dependencies
         *                        in it via Dagger.
         * */
        viewModel = ViewModelProviders.of(getActivity(), viewModelFactory).get(SearchViewModel.class);
        /**
         * @viewBinding binding the fragment with view via data binding.
         * */
        viewBinding = FragmentSearchBinding.inflate(inflater, container, false);
        viewBinding.setViewModel(viewModel);

        searchListView = viewBinding.listSearchArticles;
        searchListView.setLayoutManager(new LinearLayoutManager(getContext()));
        searchListView.setAdapter(new SearchArticleListAdapter());

        searchEditView = viewBinding.viewSearchKeyword;
        viewModel.setSearchObserver(searchEditView.listenToTextChange());

        return viewBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }
}

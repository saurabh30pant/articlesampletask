package com.saurabh.article.app.ui.app_views;

import android.content.Context;
import android.content.res.TypedArray;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.AttributeSet;


import com.saurabh.article.app.R;

import androidx.appcompat.widget.AppCompatEditText;
import io.reactivex.subjects.PublishSubject;
import io.reactivex.subjects.Subject;

import static com.saurabh.article.app.ui.app_views.AppFont.Font_Bold;
import static com.saurabh.article.app.ui.app_views.AppFont.Font_Light;
import static com.saurabh.article.app.ui.app_views.AppFont.Font_Regular;

/**
 * Created by Saurabh(aqua) on 21-03-2017.
 */

public class AppEditView extends AppCompatEditText implements TextWatcher {

    private Context context;
    private Subject<String> dataChangeSubject;

    public static final int RegularFont = 1;
    public static final int BoldFont = 2;
    public static final int LightFont = 3;

    private int fontType = RegularFont;

    public AppEditView(Context context) {
        super(context);
        this.context = context;
        createView();
    }

    public AppEditView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        TypedArray ta = getContext().obtainStyledAttributes(attrs, R.styleable.textview_font_type);
        fontType = ta.getInt(R.styleable.textview_font_type_font_type, RegularFont);
        createView();
    }

    private void createView() {
        dataChangeSubject = PublishSubject.create();
        setTypeface(FontTypeface.get(Font_Regular, context));
        setTextStyle(fontType);
        addTextChangedListener(this);
    }

    public void setTextStyle(int code){
        switch (code) {
            case RegularFont:
                setTypeface(FontTypeface.get(Font_Regular, context));
                break;
            case BoldFont:
                setTypeface(FontTypeface.get(Font_Bold, context));
                break;
            case LightFont:
                setTypeface(FontTypeface.get(Font_Light, context));
                break;
        }
    }

    public Subject<String> listenToTextChange() {
        return dataChangeSubject;
    }


    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        setSelection(s.toString().length());
        if (dataChangeSubject != null)
            dataChangeSubject.onNext(s.toString());
    }

    @Override
    public void afterTextChanged(Editable s) {}

    public void setInputAsPassword(boolean isPassword) {
        setInputType(!isPassword? (InputType.TYPE_CLASS_TEXT|InputType.TYPE_TEXT_VARIATION_PASSWORD) : (InputType.TYPE_CLASS_TEXT));
    }
}

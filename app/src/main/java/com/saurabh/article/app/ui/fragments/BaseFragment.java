package com.saurabh.article.app.ui.fragments;

import com.saurabh.article.app.models.viewmodels.ViewModelFactory;

import javax.inject.Inject;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

public abstract class BaseFragment extends Fragment {

    @Nullable
    @Inject
    ViewModelFactory viewModelFactory;

}
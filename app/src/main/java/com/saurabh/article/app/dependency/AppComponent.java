package com.saurabh.article.app.dependency;

import com.saurabh.article.app.dependency.modules.AppModule;
import com.saurabh.article.app.dependency.modules.NetworkModule;
import com.saurabh.article.app.dependency.modules.ViewModelModule;
import com.saurabh.article.app.ui.activities.ArticleListActivity;
import com.saurabh.article.app.ui.fragments.PopularFragment;
import com.saurabh.article.app.ui.fragments.SearchFragment;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {AppModule.class, ViewModelModule.class, NetworkModule.class})
public interface AppComponent {

    void inject(ArticleListActivity articleListActivity);
    void inject(SearchFragment searchFragment);
    void inject(PopularFragment popularFragment);
}

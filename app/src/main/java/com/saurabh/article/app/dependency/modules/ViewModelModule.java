package com.saurabh.article.app.dependency.modules;

import com.saurabh.article.app.controllers.contracts.ResourceMapper;
import com.saurabh.article.app.controllers.network.RemoteDataSource;
import com.saurabh.article.app.models.viewmodels.PopularViewModel;
import com.saurabh.article.app.models.viewmodels.SearchViewModel;
import com.saurabh.article.app.models.viewmodels.ViewModelFactory;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.Map;

import javax.inject.Provider;
import javax.inject.Singleton;

import androidx.lifecycle.ViewModel;
import dagger.MapKey;
import dagger.Module;
import dagger.Provides;
import dagger.multibindings.IntoMap;

@Module
public class ViewModelModule {

    @Target(ElementType.METHOD)
    @Retention(RetentionPolicy.RUNTIME)
    @MapKey
    @interface ViewModelKey {
        Class<? extends ViewModel> value();
    }

    @Provides
    ViewModelFactory viewModelFactory(Map<Class<? extends ViewModel>, Provider<ViewModel>> providerMap) {
        return new ViewModelFactory(providerMap);
    }

    @Provides
    @Singleton
    @IntoMap
    @ViewModelKey(SearchViewModel.class)
    ViewModel getSearchVM(RemoteDataSource remoteDataSource) {
        return new SearchViewModel(remoteDataSource);
    }

    @Provides
    @Singleton
    @IntoMap
    @ViewModelKey(PopularViewModel.class)
    ViewModel getPopularVM(RemoteDataSource remoteDataSource, ResourceMapper resourceMapper) {
        return new PopularViewModel(remoteDataSource, resourceMapper);
    }
}
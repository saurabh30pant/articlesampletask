package com.saurabh.article.app.controllers.contracts;

import android.graphics.drawable.Drawable;

public interface ResourceMapper {

    String getString(int resId);

    int getColor(int resId);

    Drawable getDrawable(int resId);
}

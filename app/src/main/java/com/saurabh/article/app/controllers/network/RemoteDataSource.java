package com.saurabh.article.app.controllers.network;

import com.saurabh.article.app.models.responses.PopularResponse;
import com.saurabh.article.app.models.responses.SearchResponse;

import javax.inject.Inject;

import io.reactivex.Observable;

public class RemoteDataSource {

    private API apiCall;

    @Inject
    public RemoteDataSource(API api) {
        this.apiCall = api;
    }

    public Observable<SearchResponse> search(String keyword) {
        return apiCall.search(keyword);
    }

    public Observable<PopularResponse> getMostEmailed() {
        return apiCall.mostEmailed();
    }

    public Observable<PopularResponse> getMostShared() {
        return apiCall.mostShared();
    }

    public Observable<PopularResponse> getMostViewed() {
        return apiCall.mostViewed();
    }
}